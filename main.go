package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

func main() {
	d := flag.String("d", ".", "Directory")
	p := flag.Int("p", 3000, "Port")
	flag.Parse()
	port := fmt.Sprintf(":%d", *p)

	fs := http.FileServer(http.Dir(*d))
	http.Handle("/", fs)

	log.Printf("Listening on :%d", *p)
	err := http.ListenAndServe(port, nil)
	if err != nil {
		log.Fatal(err)
	}
}
